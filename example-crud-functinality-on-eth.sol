pragma solidity ^0.4.6;

contract UserCrud {

  struct UserStruct {
    address userAddress;
    bytes32[20] userSkins;
    uint userFunds;
    uint index;
  }

  mapping(bytes32 => UserStruct) private userStructs;
  bytes32[] private userIndex;

  event LogNewUser   (address indexed userAddress, uint index, bytes32 userSteamId, uint userFunds, bytes32[20] userSkins);
  event LogUpdateUser(address indexed userAddress, uint index, bytes32 userSteamId, uint userFunds, bytes32[20] userSkins);

  function isUser(bytes32 userSteamId)
    public
    constant
    returns(bool isIndeed)
  {
    if(userIndex.length == 0) return false;
    return (userIndex[userStructs[userSteamId].index] == userSteamId);
  }

  function insertUser(
    address userAddress,
    bytes32 userSteamId,
    uint userFunds,
    bytes32[20] userSkins)
    public
    returns(uint index)
  {
    if(isUser(userSteamId)) revert();
    userStructs[userSteamId].userAddress = userAddress;
    userStructs[userSteamId].userSkins   = userSkins;
    userStructs[userSteamId].userFunds   = userFunds;
    userStructs[userSteamId].index     = userIndex.push(userSteamId)-1;
    LogNewUser(
        userAddress,
        userStructs[userSteamId].index,
        userSteamId,
        userFunds,
        userSkins);
    return userIndex.length-1;
  }

  function getUser(bytes32 userSteamId)
    public
    constant
    returns(address userAddress, bytes32[20] userSkins, uint userFunds, uint index)
  {
    if(!isUser(userSteamId)) revert();
    return(
      userStructs[userSteamId].userAddress,
      userStructs[userSteamId].userSkins,
      userStructs[userSteamId].userFunds,
      userStructs[userSteamId].index);
  }


  function updateUserSkins(bytes32 userSteamId, bytes32[20] userSkins)
    public
    returns(bool success)
  {
    if(!isUser(userSteamId)) revert();
    userStructs[userSteamId].userSkins = userSkins;
    LogUpdateUser(
      userStructs[userSteamId].userAddress,
      userStructs[userSteamId].index,
      userSteamId,
      userStructs[userSteamId].userFunds,
      userSkins);
    return true;
  }

    function updateUserFunds(bytes32 userSteamId, uint userFunds)
    public
    returns(bool success)
    {
        if(!isUser(userSteamId)) revert();
        userStructs[userSteamId].userFunds = userFunds;
        LogUpdateUser(
          userStructs[userSteamId].userAddress,
          userStructs[userSteamId].index,
          userSteamId,
          userFunds,
          userStructs[userSteamId].userSkins);
        return true;
    }

  function getUserCount()
    public
    constant
    returns(uint count)
  {
    return userIndex.length;
  }

  function getUserAtIndex(uint index)
    public
    constant
    returns(bytes32 userSteamId)
  {
    return userIndex[index];
  }

}
