var steamUserInventory = require("steam-user-inventory");
var steamBot = require("../bots/index.js");
var User = require("../models/userModel.js");
var opPrices = require("../models/opPricesModel.js");
var botModel = require("../models/botModel.js");
var ethController = require("./ethController");

var sdsBot4 = new steamBot({
    accountName: "example-account",
    password: "example-password"
});


sdsBot4.manager.on("sentOfferChanged", offer => {
    if (offer.state === 3) {
        if (offer.itemsToGive === null || offer.itemsToGive.length === 0) {
            // the bot receives items which means itemsToGive is null and a user deposit item to the website
            var itemPrice;
            User.findOne({_id: offer.partner}, function(err, result) {
                console.log(result.offersPending);
                for (item in result.offersPending) {
                    if (result.offersPending[item].offerId === offer.id) {
                        itemPrice = result.offersPending[item].itemPrice;
                        offer.getReceivedItems(function(err, items) {
                            var newBot = new botModel({
                                _id: items[0].id,
                                price: itemPrice,
                                image: items[0].getImageURL(),
                                owner: offer.partner,
                                item: items[0]
                            });
                            newBot.save(function(err) {
                                if (err) console.log(err);
                            });

                            User.update(
                                {_id: offer.partner},
                                {
                                    $push: {
                                        skinsDeposited: {
                                            _id: items[0].id,
                                            image: items[0].getImageURL(),
                                            price: itemPrice,
                                            item: items[0]
                                        }
                                    }
                                },
                                function(err, result) {
                                    if (!err) {
                                        // update their skins on the blockchain
                                        ethController.updateUserEthSkins(String(offer.partner),false);
                                    }
                                }
                            );
                            User.update(
                                {_id: offer.partner},
                                {
                                    $pull: {
                                        offersPending: {offerId: offer.id}
                                    }
                                },
                                function(err) {
                                    console.error(err);
                                }
                            );
                        });
                    }
                }
            });
        } else if (
            offer.itemsToReceive === null || // A user has bought something or is withdrawing an item
            offer.itemsToReceive.length === 0
        ) {
            for (item in offer.itemsToGive) {
                botModel.findOne({_id: offer.itemsToGive[item].id}, function(
                    err,
                    res
                ) {
                    User.findOne({_id: res.owner}, function(err, result) {
                        var skinPrice;
                        for (var skin in result.skinsDeposited) {
                            if (
                                result.skinsDeposited[skin]._id ===
                                offer.itemsToGive[item].id
                            ) {
                                skinPrice = result.skinsDeposited[
                                    skin
                                ].price;
                                // remove the skin that the seller has deposited and increment their money
                                User.update(
                                    {_id: res.owner},
                                    {
                                        $pull: {
                                            skinsDeposited: {
                                                _id: offer.itemsToGive[item].id
                                            }
                                        },
                                        $inc: {
                                            funds: skinPrice
                                        }
                                    },
                                    {safe: true, multi: true},
                                    function(err) {
                                        if (!err) {
                                            ethController.updateUserEthSkins(String(res.owner),true); // update the skins on the blockchain
                                        }
                                    }
                                );
                                // decrement the funds of the buyr and update their skins on the blockchain 
                                var decrementFunds = "-" + skinPrice;
                                User.update(
                                    {_id: offer.partner},
                                    {$inc: {funds: decrementFunds}},
                                    function(err) {
                                        if (!err) {
                                            ethController.updateUserEthSkins(String(offer.partner),true);
                                        }
                                    }
                                );
                            }
                        }
                    });
                    botModel
                        .findOne({_id: offer.itemsToGive[item].id})
                        .remove(function(err) {
                            if (err) {
                                console.error(err);
                            }
                        });
                });
            }
        }
    }
});

module.exports = {
    deposit(req, res) {
        req.body._id = req.user.id;
        var items = [];
        items.push(req.body.item);
        User.findOne({_id: req.body._id}, function(err, result) {
            if (!err) {
                sdsBot4.depositItem(req.user.id, items, result.tradeLink);
                res.send("DEPOSIT UNDERWAY");
            }
        });

        console.log("Steam items inserted into skins collection");
    },

    withdraw(req, res) {
        req.body._id = req.user.id;
        var items = [];
        items.push(req.body.item);
        User.findOne({_id: req.body._id}, function(err, result) {
            if (!err) {
                sdsBot4.withdrawItem(items, result.tradeLink);
                res.send("WITHDRAWAL UNDERWAY");
            }
        });
    },

    getDepositedItems(req, res) {
        botModel.find({}, function(err, result) {
            if (!err) {
                res.send({items: result});
            }
        });
    },

    getSkinsDeposited(req, res) {
        User.findOne({_id: req.user.id}, function(err, result) {
            if (!err) {
                res.send({skinsDeposited: result.skinsDeposited});
            }
        });
    }
};
